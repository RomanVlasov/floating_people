from collections import Counter
import operator


def get_final_choices(choices_made):
    result = list()
    for item in sorted(choices_made.items(), key=operator.itemgetter(0)):
        for choice in item[1]:
            if isinstance(choice, tuple):
                name, choice_type = choice
                if choice_type == "final":
                    result.append(name)
    return result


def get_most_popular_choices(counter):
    result = counter.most_common(1)
    max_elem_count = result[0][1]
    for item in counter.most_common()[1:]:
        if item[1] == max_elem_count:
            result.append(item)
    return result


def get_preferred_name(choices_made):
    final_choices = get_final_choices(choices_made)
    most_popular = get_most_popular_choices(Counter(final_choices))
    if len(most_popular) == 1:
        return most_popular[0]
    if most_popular[0][1] == 1:
        return final_choices[-1], 1
    try:
        return next(x for x in most_popular if x[0] == final_choices[-1])
    except StopIteration:
        return most_popular[-1]







