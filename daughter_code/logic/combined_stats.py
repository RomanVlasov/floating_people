import os
from daughter_code.logic.user_statistics import UserStatistics

class PlotData():
    def __init__(self):
        self.y_values = list()
        self.label = 'Unknown'


class CombinedStats():
    def __init__(self):
        self._stats = list()

    def load(self, dir):
        for root, dirs, files in os.walk(dir):
            for file in files:
                if file == "stats.json":
                    self._add(os.path.join(root, file))

    def _add(self, filepath):
        stats = UserStatistics(None)
        stats.load_from_file(filepath)
        last_dir = os.path.basename(os.path.dirname(filepath))
        stats.set_username(last_dir)
        print("Adding stats for user: '{}'".format(stats.username))
        self._stats.append(stats)

    def get_plot_data(self):
        result = list()
        for stats in self._stats:
            data = PlotData()
            data.label = stats.username
            data.y_values = stats.get_choices_plot()
            result.append(data)
        return result

