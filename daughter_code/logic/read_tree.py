from resources.text import read_chain_intro
from resources.text import read_chain_wife
from resources.text import read_chain_hunchback
from resources.text import read_chain_boy 
from resources.text import read_chain_bus
from resources.text import read_chain_nav
from resources.text import read_chain_music
from resources.text import read_chain_train 
from resources.text import read_chain_last


tree = [ 
{'type': 'simple', 'chain': read_chain_intro.text_start},
{'type': 'choice', 
    'choices':
    {'Агния': read_chain_intro.text_a_1,
    'Мара': read_chain_intro.text_a_2,
    'Тамила': read_chain_intro.text_a_3,
    'Юна': read_chain_intro.text_a_4}
},
{'type': 'simple', 'chain': read_chain_wife.text_wife},
{'type': 'choice', 
    'choices':
    {'Агния': read_chain_wife.text_wife_1,
    'Мара': read_chain_wife.text_wife_2,
    'Тамила': read_chain_wife.text_wife_3,
    'Юна': read_chain_wife.text_wife_4}
},
{'type': 'simple', 'chain': read_chain_hunchback.text_gorbaty},
{'type': 'choice', 
    'choices':
    {'Агния': read_chain_hunchback.text_gorbaty_1,
    'Мара': read_chain_hunchback.text_gorbaty_2,
    'Тамила': read_chain_hunchback.text_gorbaty_3,
    'Юна': read_chain_hunchback.text_gorbaty_4}
},
{'type': 'simple', 'chain': read_chain_boy.text_boy},
{'type': 'choice', 
    'choices':
    {'Агния': read_chain_boy.text_boy_1,
    'Мара': read_chain_boy.text_boy_2,
    'Тамила': read_chain_boy.text_boy_3,
    'Юна': read_chain_boy.text_boy_4}
},
{'type': 'simple', 'chain': read_chain_bus.text_bus_appear},
{'type': 'analysis',
 'choices':
     {'Агния': read_chain_bus.analysis_1,
      'Мара': read_chain_bus.analysis_2,
      'Тамила': read_chain_bus.analysis_3,
      'Юна': read_chain_bus.analysis_4
      },
},
{'type': 'simple', 'chain': read_chain_bus.text_bus_arrive},
{'type': 'choice', 
    'choices':
    {'Агния': read_chain_bus.text_1,
    'Мара': read_chain_bus.text_2,
    'Тамила': read_chain_bus.text_3,
    'Юна': read_chain_bus.text_4}
},
{'type': 'simple', 'chain': read_chain_bus.text_after},
{'type': 'simple', 'chain': read_chain_nav.text},
{'type': 'choice', 
    'choices':
    {'Агния': read_chain_nav.text_1,
    'Мара': read_chain_nav.text_2,
    'Тамила': read_chain_nav.text_3,
    'Юна': read_chain_nav.text_4}
},
{'type': 'simple', 'chain': read_chain_music.text},
{'type': 'choice', 
    'choices':
    {'Агния': read_chain_music.text_1,
    'Мара': read_chain_music.text_2,
    'Тамила': read_chain_music.text_3,
    'Юна': read_chain_music.text_4}
},
{'type': 'simple', 'chain': read_chain_train.text},
{'type': 'choice', 
    'choices':
    {'Агния': read_chain_train.text_1,
    'Мара': read_chain_train.text_2,
    'Тамила': read_chain_train.text_3,
    'Юна': read_chain_train.text_4}
},
{'type': 'simple', 'chain': read_chain_last.text},
{'type': 'choice', 
    'choices':
    {'Агния': read_chain_last.text_1,
    'Мара': read_chain_last.text_2,
    'Тамила': read_chain_last.text_3,
    'Юна': read_chain_last.text_4}
},
{'type': 'simple', 'chain': read_chain_last.text_after},
{'type': 'show_graph'}
]

def test_graph_tree():
    return [ {'type': 'simple', 'chain': ['А теперь график']},
            {'type': 'show_graph'}]

def read_tree():
    return tree