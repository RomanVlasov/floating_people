from daughter_code.logic.read_tree import read_tree, test_graph_tree
from daughter_code.logic.user_statistics import UserStatistics
from daughter_code.logic import analyze_stat
from daughter_code.logic.text_processor import processor as text_processor

class SceneryTree:
    def __init__(self, on_choice, on_select, on_show_graph):
        self.tree = read_tree()
        self.on_choice = on_choice
        self._on_select = on_select
        self._on_show_graph = on_show_graph

        # История выборов
        self.text = list()
        self.i = -1

        # Работа с дерервьями
        self.tree_part = 0
        self.go_next()

        #выборы
        self._choices_made = dict()

    def go_next(self, debug_skip_all_text=False):
        if (self.i == len(self.text) - 1) or debug_skip_all_text:
            self.i = len(self.text)
            self._next_tree()
        elif self.i < len(self.text) - 1:
            self.i += 1

    def _next_tree(self):
        if self.is_finished():
            self.i -= 1
            return

        tree_part = self.tree[self.tree_part]
        if tree_part["type"] == "choice":
            self.on_choice()
        elif tree_part["type"] == "show_graph":
            self._on_show_graph()
            self.tree_part += 1
        elif tree_part["type"] == "analysis":
            text_to_add = tree_part["choices"][analyze_stat.get_preferred_name(self._choices_made)[0]]
            self._add_text_choices_format(text_to_add)
            self.tree_part += 1
        else:
            self.text += tree_part["chain"]
            self.tree_part += 1

    def get_text(self):
        return self.text[self.i if self.i < len(self.text) else len(self.text) - 1]

    def go_prev(self):
        if self.i > 0:
            self.i -= 1

    def choose(self, name):
        choices_made = self._choices_made.setdefault(self.tree_part, list())
        if name in choices_made:
            choices_made[choices_made.index(name)] = (name, "final")
            self.tree_part += 1
            self._next_tree()
        else:
            choices_made.append(name)
            tree_part = self.tree[self.tree_part]
            self.text += ["[b]# {}[/b]\n".format(name) + x for x in tree_part['choices'][name]]
        self._on_select()

    def get_cur_choices(self):
        return self._choices_made.setdefault(self.tree_part, list())

    def make_stats(self, username):
        result = UserStatistics(self._choices_made)
        result.set_username(username)
        return result

    def is_finished(self):
        return self.tree_part >= len(self.tree)

    def _add_text_choices_format(self, new_text):
        prefix = text_processor.form_prefix_for_history(analyze_stat.get_final_choices(self._choices_made))
        self.text += [prefix + "\n" + x for x in new_text]