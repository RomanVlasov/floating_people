import unittest
from daughter_code.logic.analyze_stat import get_preferred_name


def form_history_dict(stat):
    result = dict()
    for i in range(len(stat)):
        result[i] = stat[i]
    return result


class GetPreferredNameTest(unittest.TestCase):
    def test_simple_stat(self):
        test_stat = [["Агния", "Мара", ("Агния", "final")]]
        self.assertEquals(("Агния", 1), get_preferred_name(form_history_dict(test_stat)))

    def test_multiple_items(self):
        test_stat = [["Агния", "Мара", ("Агния", "final")], [("Агния", "final")], [("Мара", "final")]]
        self.assertEquals(("Агния", 2), get_preferred_name(form_history_dict(test_stat)))

    def test_equal_counts(self):
        test_stat = [[("Агния", "final")], [("Агния", "final")], [("Мара", "final")], [("Мара", "final")]]
        self.assertEquals(("Мара", 2), get_preferred_name(form_history_dict(test_stat)))

    def test_equal_counts_case_2(self):
        test_stat = [[("Агния", "final")], [("Мара", "final")], [("Мара", "final")], [("Агния", "final")]]
        self.assertEquals(("Агния", 2), get_preferred_name(form_history_dict(test_stat)))

    def test_equal_counts_case_3(self):
        test_stat = [[("Агния", "final")], [("Мара", "final")], [("Мара", "final")], [("Агния", "final")], [("Тамила", "final")]]
        self.assertEquals(2, get_preferred_name(form_history_dict(test_stat))[-1])

    def test_one_count_stat(self):
        test_stat = [[("Агния", "final")], [("Мара", "final")]]
        self.assertEquals(("Мара", 1), get_preferred_name(form_history_dict(test_stat)))