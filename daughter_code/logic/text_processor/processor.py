

def form_prefix_for_history(history_names):
    combined_str = "-".join(history_names)
    return "[b][{}][/b]".format(combined_str)
