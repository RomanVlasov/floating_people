import os
import json
import codecs
from random import randint


class UserStatistics():
    STATS_DICT = {
        'Агния': 4,
        'Мара': 3,
        'Тамила': 2,
        'Юна': 1
    }

    def __init__(self, choices_history):
        self._choices_history = choices_history
        self._user_name = "user" + str(randint(0, 40))

    @property
    def username(self):
        return self._user_name

    def set_username(self, name):
        self._user_name = name

    def save_to_file(self, file_path):
        os.makedirs(os.path.dirname(file_path), exist_ok=True)
        with codecs.open(file_path, mode = 'w', encoding='utf-8') as file:
            res_dict = dict(
                user=self._user_name,
                history=self._choices_history
            )
            json.dump(res_dict, file, ensure_ascii=False)

    def load_from_file(self, file_path):
        with codecs.open(file_path, mode='r', encoding='utf-8') as file:
            res_dict = json.load(file)
            self._user_name = res_dict["user"]
            self._choices_history = res_dict["history"]

    def get_choices_plot(self):
        def weight(key):
            return int(key[0])

        result = list()
        for key, item in sorted(self._choices_history.items(), key=weight):
            result.append(self._name_index(self._get_final_choice(item)))
        return result

    def get_final_choice(self):
        final = self._get_final_choice(self._choices_history[-1])
        return final

    def _name_index(self, name):
        return self.STATS_DICT[name]

    def _get_final_choice(self, history_item):
        for choice in history_item:
            if isinstance(choice, tuple):
                return choice[0]
            if isinstance(choice, list):
                return choice[0]
        raise RuntimeError("Wrong history item format: {}".format(history_item))


if __name__ == "__main__":
    history = {2: ['Агния', ('Мара', "final")],
               5: ['Агния', ('Мара', "final")]}

    stats = UserStatistics(history)
    stats.set_username('Рута Мейлутите')
    filepath = os.path.join("stats", stats.username + ".json")
    stats.save_to_file(filepath)

    stats2 = UserStatistics(None)
    stats2.load_from_file(filepath)

    print(stats.username)
    print(stats2.username)
    print(stats2._choices_history)