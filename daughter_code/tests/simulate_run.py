import os.path
from random import randint
from daughter_code.logic.scenery_tree import SceneryTree
from daughter_code.logic.combined_stats import CombinedStats

NAME_LIST = ['Агния', 'Мара', 'Тамила', 'Юна']

class TestSimulateRun():
    def __init__(self):
        self._scenery = SceneryTree(
            on_choice=self._on_choice_appear,
            on_select=self._on_selection,
            on_show_graph=self._show_graph
        )

    def run(self):
        while not self._scenery.is_finished():
            self._scenery.go_next()

    def _on_choice_appear(self):
        choice = NAME_LIST[randint(0, 3)]
        print("Choosing {}".format(choice))
        self._scenery.choose(choice)

    def _on_selection(self):
        print("Tree part: {}".format(self._scenery.tree_part))
        print("Process selection")

    def _show_graph(self):
        print("Show user statistics graph")
        username = "Леонид Горбовский"
        stats = self._scenery.make_stats(username)
        stats_path = os.path.join("stats", username, "stats.json")
        stats.save_to_file(stats_path)
        self._read_all_stats()

    def _read_all_stats(self):
        all_stats = CombinedStats()
        all_stats.load("stats")
        self._show_ui_graph(all_stats.get_plot_data())

    def _show_ui_graph(self, plot_data):
        from daughter_code.ui.choices_graph import show_graph
        show_graph(plot_data)




if __name__ == "__main__":
    TestSimulateRun().run()