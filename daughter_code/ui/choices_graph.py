from random import randint
from kivy.uix.anchorlayout import AnchorLayout


class ChoicesGraph(AnchorLayout):
	pass


def xsticks():
	return ["Письмо", "Размышление", "Горбатый", "Мальчуган",
			"Водитель", "Радость", "Музыка", "Попутчик", "Финал"]


def test_data():
	y_data = [1, 2, 1, 3, 4, 4, 3, 4, 4]
	y_labels = ['', 'Агния', 'Мара', 'Тамила', 'Юна', '']
	return y_labels, y_data


def import_matplotlib():
	import matplotlib
	matplotlib.use('module://kivy.garden.matplotlib.backend_kivy')

def show_graph_default():
	import matplotlib.pyplot as plt
	from matplotlib import rc
	from matplotlib.font_manager import FontProperties
	import numpy as np

	font = {'family': 'Verdana',
        'weight': 'normal'}
	rc('font', **font)
	#x_values = [0.1, 0.3, 0.4, 0.2]
	#y_values = ["word 1", "word 2", "word 3", "word 4"]

	y_axis = np.arange(0, 6, 1)

	x_values = np.arange(1, 10, 1)
	y_labels, y_values = test_data()

	#plt.barh(y_axis, x_values, align='center')
	plt.plot(x_values, y_values, label='Роман Власов')
	for x in range(0, 5):
		plt.plot(x_values, [randint(1, 4) for x in y_values], label='Анна Власова')

	fontP = FontProperties()
	fontP.set_size('xx-small')
	plt.legend(prop=fontP)

	plt.yticks(y_axis, y_labels)
	plt.xticks(x_values, xsticks(), rotation='vertical')

	plt.show()

def show_graph(plot_data):
	import matplotlib.pyplot as plt
	from matplotlib import rc
	from matplotlib.font_manager import FontProperties
	import numpy as np

	font = {'family': 'Verdana',
			'weight': 'normal'}
	rc('font', **font)

	y_axis = np.arange(0, 6, 1)
	y_labels = ['', 'Юна', 'Тамила', 'Мара', 'Агния', '']

	x_values = np.arange(1, 10, 1)

	for data in plot_data:
		y_values = data.y_values
		plt.plot(x_values, y_values, label=data.label)

	fontP = FontProperties()
	fontP.set_size('xx-small')
	plt.legend(prop=fontP)

	plt.yticks(y_axis, y_labels)
	plt.xticks(x_values, xsticks(), rotation=45)

	plt.tight_layout()
	plt.show()


if __name__ == "__main__":
	#import_matplotlib()
	show_graph_default()



