import kivy
kivy.require('1.9.0')

from kivy.app import App, Builder
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.anchorlayout import AnchorLayout
from daughter_code.logic.scenery_tree import SceneryTree
from show_stats import show_stats

import os.path

Builder.load_file('choices_dialog.kv')
Builder.load_file('text_window.kv')


class RootWidget(AnchorLayout):
    pass


class ChoicesDialog(AnchorLayout):
    pass


class FloatingPeopleApp(App):
    def build(self):
        self._text_chain = SceneryTree(
            on_choice=self.to_choice_window,
            on_select=self.to_select,
            on_show_graph=self._to_show_graph)

        self._wrapper = FloatLayout()
        self._update_root()

        return self._wrapper

    def go_prev(self):
        self._text_chain.go_prev()
        self._update_text()

    def go_prev_from_choice(self):
        self._text_chain.go_prev()
        self._update_root()

    def go_next(self):
        if self._text_chain.is_finished():
            self.stop()
            return

        self._text_chain.go_next(debug_skip_all_text=False)
        self._update_text()

    def on_choose(self, button):
        self._text_chain.choose(button.text)

    def to_choice_window(self):

        dialog = ChoicesDialog()
        box = dialog.ids.choice_box
        for child in box.children:
            if child.text in self._text_chain.get_cur_choices():
                child.background_color = (1.0, 0.0, 0.0, 1.0)

        self._wrapper.clear_widgets()
        self._wrapper.add_widget(dialog)

    def _to_show_graph(self):
        self._make_stats()

    def to_select(self):
        self._update_root()

    def _update_text(self):
        self._main_text_widget.text = self._text_chain.get_text()

    def _update_root(self):
        self._wrapper.clear_widgets()
        self._root = RootWidget()
        self._main_text_widget = self._root.ids.main
        self._wrapper.add_widget(self._root)
        self._update_text()

    def _make_stats(self):
        username = "Последний запуск"
        stats = self._text_chain.make_stats(username)
        stats_path = os.path.join("stats", username, "stats.json")
        stats.save_to_file(stats_path)

        from random import randint
        username = "Последний запуск {}".format(randint(0, 70000))
        stats = self._text_chain.make_stats(username)
        stats_path = os.path.join("stats_backup", username, "stats.json")
        stats.save_to_file(stats_path)


if __name__ == "__main__":
    FloatingPeopleApp().run()
    show_stats()
