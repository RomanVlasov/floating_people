from daughter_code.logic.combined_stats import CombinedStats
from daughter_code.ui.choices_graph import show_graph

def show_stats():
    combined_stats = CombinedStats()
    combined_stats.load("stats")
    show_graph(combined_stats.get_plot_data())

if __name__ == "__main__":
    show_stats()